#include <jni.h>
#include <string>
#include "opencv2/opencv.hpp"
#include <android/native_window_jni.h>
ANativeWindow *window = 0;
using namespace cv;
DetectionBasedTracker *tracker = 0;

extern "C" JNIEXPORT jstring JNICALL
Java_com_lg_opencvfacerecognition_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}



class CascadeDetectorAdapter: public DetectionBasedTracker::IDetector
{
public:
    CascadeDetectorAdapter(cv::Ptr<cv::CascadeClassifier> detector):
            IDetector(),
            Detector(detector)
    {
    }

    void detect(const cv::Mat &Image, std::vector<cv::Rect> &objects)
    {
        Detector->detectMultiScale(Image, objects, scaleFactor, minNeighbours, 0, minObjSize, maxObjSize);
    }

    virtual ~CascadeDetectorAdapter()
    {
    }

private:
    CascadeDetectorAdapter();
    cv::Ptr<cv::CascadeClassifier> Detector;
};


extern "C"
JNIEXPORT void JNICALL
 Java_com_lg_opencvfacerecognition_OpencvJni_init(JNIEnv *env, jobject instance, jstring path_) {

    const char *path = env->GetStringUTFChars(path_, 0);

    //1.检测器
    //创建检测器    classifier RecyclerView    CascadeDetectorAdapter  适配器
    Ptr<CascadeClassifier> classifier=makePtr<CascadeClassifier>(path);
    Ptr<CascadeDetectorAdapter>   mainDetector= makePtr<CascadeDetectorAdapter>(classifier);
    //2.跟踪器
    Ptr<CascadeClassifier> classifier1=makePtr<CascadeClassifier>(path);
    Ptr<CascadeDetectorAdapter>   trackingDetector= makePtr<CascadeDetectorAdapter>(classifier1);

//    tracker  含有两个对象 检测器 跟踪器
    DetectionBasedTracker::Parameters DetectorParams;
    tracker= new DetectionBasedTracker(mainDetector, trackingDetector, DetectorParams);
    tracker->run();
    env->ReleaseStringUTFChars(path_, path);
 }

  extern "C"
  JNIEXPORT void JNICALL
  Java_com_lg_opencvfacerecognition_OpencvJni_postData(JNIEnv *env, jobject instance,
                                                       jbyteArray data_, jint w, jint h,
                                                       jint cameraId) {
      //    yuv  y1920*1080   u 1080/4  v  1080/4  w  h 1       NV21 --->Bitmap  --->  Mat   y+u/4+v/v
      jbyte *data = env->GetByteArrayElements(data_, NULL);

      //    src   ==Bitmap
      Mat src(h + h / 2, w, CV_8UC1, data);
      cvtColor(src, src, COLOR_YUV2RGBA_NV21);
      //旋转摄像头
      if(cameraId==1){
          //imwrite("/storage/emulated/0/src.jpg", src);
          //设置为前置摄像头
          rotate(src, src, ROTATE_90_COUNTERCLOCKWISE);
          //镜像反转
          flip(src, src, 1);
      }else{
          //顺时针旋转90度
          rotate(src, src, ROTATE_90_CLOCKWISE);
      }
      Mat gray;
      cvtColor(src, gray, COLOR_RGBA2GRAY);
      //    对比度    黑白  轮廓 二值化
      equalizeHist(gray, gray);
//    检测人脸
      std::vector<Rect> faces;
      tracker->process(gray);
      tracker->getObjects(faces);

      for (Rect face : faces) {
          rectangle(src, face, Scalar(255, 0, 255));

      }
      //    Mat   src
      if (window) {
          ANativeWindow_setBuffersGeometry(window, src.cols, src.rows, WINDOW_FORMAT_RGBA_8888);
          ANativeWindow_Buffer window_buffer;
          do{
              //lock失败 直接brek出去
              if (ANativeWindow_lock(window, &window_buffer, 0)) {
                  ANativeWindow_release(window);
                  window = 0;
                  break;
              }
              //src.data ： rgba的数据
              //把src.data 拷贝到 buffer.bits 里去
              // 一行一行的拷贝
              //填充rgb数据给dst_data
              uint8_t *dst_data = static_cast<uint8_t *>(window_buffer.bits);
              //stride : 一行多少个数据 （RGBA） * 4
              int dst_linesize = window_buffer.stride * 4;

              //一行一行拷贝
              for (int i = 0; i < window_buffer.height; ++i) {
                  memcpy(dst_data + i * dst_linesize, src.data + i * src.cols * 4, dst_linesize);
              }
              //提交刷新
              ANativeWindow_unlockAndPost(window);


          }while (0);



      }
      src.release();
      gray.release();
      env->ReleaseByteArrayElements(data_, data, 0);
  }

extern "C"
JNIEXPORT void JNICALL
Java_com_lg_opencvfacerecognition_OpencvJni_setSurface(JNIEnv *env, jobject instance,
                                                       jobject surface) {
    if (window) {
        ANativeWindow_release(window);
        window = 0;
    }
    window = ANativeWindow_fromSurface(env, surface);


}