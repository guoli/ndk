package com.lg.opencvfacerecognition;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;



import com.lg.opencvfacerecognition.util.CameraHelper;
import com.lg.opencvfacerecognition.util.Utils;

import java.io.File;

public class StartActivity extends AppCompatActivity implements SurfaceHolder.Callback, Camera.PreviewCallback {

    // Used to load the 'native-lib' library on application startup.
    private OpencvJni openCvJni;
    private CameraHelper cameraHelper;
    int cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;

//     人脸识别     后面  opengl
//    FaceDetector faceDetector
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openCvJni = new OpencvJni();
        SurfaceView surfaceView = findViewById(R.id.surfaceView);
        surfaceView.getHolder().addCallback(this);
        cameraHelper = new CameraHelper(cameraId);
        cameraHelper.setPreviewCallback(this);
        Utils.copyAssets(this, "lbpcascade_frontalface.xml");
    }

    @Override
    protected void onResume() {
        super.onResume();
        String path = new File(Environment.getExternalStorageDirectory(), "lbpcascade_frontalface.xml").getAbsolutePath();
        cameraHelper.startPreview();
        openCvJni.init(path);
    }

    public void switchCamera(View view) {
        cameraHelper.switchCamera();
        cameraId = cameraHelper.getCameraId();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        openCvJni.setSurface(holder.getSurface());
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

//    boolean isTrue = true;
//data   rgb   1   nv21  opencv   旋转的库        2
    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        if (isTrue) {
//            isTrue = false;
            openCvJni.postData(data, CameraHelper.WIDTH, CameraHelper.HEIGHT, cameraId);
//        }

    }

    public void onDetect(View view) {
    }
}
