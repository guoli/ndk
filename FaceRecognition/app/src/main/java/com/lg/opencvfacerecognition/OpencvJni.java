package com.lg.opencvfacerecognition;

import android.view.Surface;

public class OpencvJni {
    static {
        System.loadLibrary("native-lib");
    }
    public  native  void init(String path) ;

    public native void postData(byte[] data, int width, int height, int cameraId);

    public native  void setSurface(Surface surface);

}
